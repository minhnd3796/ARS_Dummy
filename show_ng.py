from __future__ import print_function
import DummyNet_input
import cv2
import numpy as np
import matplotlib.pyplot as plt
import random

_, test = DummyNet_input.getImages()

# print((test[0][1][0]).astype(np.int))

ng_idx = []

for i in range(len(test)):
    if test[i][1][0].astype(np.int) == 1:
        ng_idx.append(i)

ng_test_idx = random.choice(ng_idx)
# print(ng_test_idx)

# num_test_all: 2604
# num_test_ng: 207

# grey_img = test[ng_test_idx][0].reshape((48, 48)) * 255.0
grey_img = test[ng_test_idx][0] * 255.0
grey_img = grey_img.astype(np.uint8)
# imgplot = plt.imshow(grey_img, cmap='gray')
# plt.show()

cv2.imshow('NG #' + str(ng_test_idx), grey_img)
cv2.waitKey(0)
cv2.destroyAllWindows()
