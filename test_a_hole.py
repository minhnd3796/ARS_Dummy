import DummyNet
import cv2
import sys
import numpy as np

def infer(filename):
    model = DummyNet.DummyNet(None, None, test_data=None, input_size=42, num_class=2, init_lnr=1e-1, depth=3 * 12 + 4,
                          bc_mode=False, reduction=1.0, weight_decay=1e-4, total_blocks=3, lnr_update_center=0.5,
                          weight_center_loss=0.0, growth_rate=12, reduce_lnr=[70, 143, 180],
                          current_save_folder='./save/DummyNet/', logs_folder='./summary/DummyNet',
                          valid_save_folder='./save/DummyNet/valid/',
                          max_to_keep=0, snapshot_test=False)
    img = cv2.resize(cv2.imread(filename, 0) / 255.0, (42, 42))[np.newaxis, :, :, np.newaxis]
    ret_val = model.test_a_hole(img)
    if int(ret_val) == 1:
        return "Image is OK!"
    else:
        return "Image is NG!"