from test_a_hole import infer
from tkinter import Tk
from tkinter.filedialog import askopenfilename
from tkinter import messagebox

Tk().withdraw() # we don't want a full GUI, so keep the root window from appearing
filename = askopenfilename() # show an "Open" dialog box and return the path to the selected file

messagebox.showinfo(title="Result", message=infer(filename))